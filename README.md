Flyshot SDK
====================

Plugin to integrate Flyshot SDK to flutter project. 
This plugin based on the native [IOS Flyshot SDK](https://bitbucket.org/flyshot/ios-sdk)

Installation Guide
====================

1. Depend on it

Add this to your package's pubspec.yaml file:

    dependencies:
      flyshot_sdk: ^1.0.0 

2. Install it

You can install packages from the command line:

with pub:

    $ pub get

with Flutter:

    $ flutter pub get

Alternatively, your editor might support pub get or flutter pub get. Check the docs for your editor to learn more.

Note: Do not forget run `pod update` after package installation. 
Make sure that your minimum target version in your `Podfile` is `10.0` or higher.

3. Import it

Now in your Dart code, you can use:

    import 'package:flyshot_sdk/flyshot_sdk.dart';


Usage
====================

SDK initialization
------------------

To initialize SDK call initialize method with token generated in your Flyshot account:

    const sdkToken = '...';
    Flyshot.initialize(sdkToken): Future


To start SDK call start method:

    Flyshot.start();


Are you using AppsFlyer to track events? Just use setAppsFlyerId method to set AppsFlyer Id:

    const appsflyerId = '...';
    Flyshot.setAppsFlyerId(appsflyerId);


Public interface
------------------

Check user_id session variable. Return true if Flyshot user_id session variable is set:

    Flyshot.isFlyshotUser(): Future<bool>


Check active campaign session variable. Return true if Flyshot active campaign session variable is set:

    Flyshot.isEligibleForDiscount(): Future<bool>

    Flyshot.isEligibleForPromo(): Future<bool>

    Flyshot.isEligibleForReward(): Future<bool>

Check if campaign is active:

    const productId = '...'; // or null if product is not present
    Flyshot.isCampaignActive(productId): Future<bool>


Send Checkout Event to Flyshot Analytics:

    Flyshot.sendConversionEvent(double amount, [String productId, String currency]): Future

Note: You can pass null instead of productId and pass null instead of currency (USD will be used)

Send Custom Event to Flyshot Analytics:

    Flyshot.sendEvent(String eventName): Future

Send Sign Up Event to Flyshot Analytics:

    Flyshot.sendSignUpEvent(): Future

Set list of product ids which should not processed:

    Flyshot.setBlockedPurchasesList(Array<String>): Future

Open promo board manually:

    Flyshot.requestPromoBanner(): Future

Upload screenshot (response will return campaign search status: 0 - not found, 1 - found, 2 - redeemed):

    Flyshot.upload(): Future<int>

Get available content for your application:

    Flyshot.getContent(): Future<dynamic>


Events
-------------------------

You should use a callback function to subscribe Flyshot events. For example:

      Flyshot.onFlyshotPurchase((params) async {
        log("onFlyshotPurchase event listener invoked with params " + params.toString());
      });

      Flyshot.onFlyshotCampaignDetected((params) async {
        log("onFlyshotCampaignDetected event listener invoked with params " + params.toString());
      });
