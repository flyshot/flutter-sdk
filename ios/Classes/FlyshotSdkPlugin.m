#import <Foundation/Foundation.h>
#import <StoreKit/StoreKit.h>

#import "FlyshotSdkPlugin.h"

@implementation FlyshotSdkPlugin

FlutterMethodChannel * channel;
NSArray * blockedPurchases = nil;

+ (void)registerWithRegistrar:(NSObject<FlutterPluginRegistrar>*)registrar {
  channel = [FlutterMethodChannel
      methodChannelWithName:@"flyshot_sdk"
            binaryMessenger:[registrar messenger]];
    
  FlyshotSdkPlugin* instance = [[FlyshotSdkPlugin alloc] init];
  [registrar addMethodCallDelegate:instance channel:channel];
}

- (NSArray<NSString *> *)supportedEvents
{
    return @[@"onFlyshotPurchase", @"onFlyshotCampaignDetected"];
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        [Flyshot shared].delegate = self;
    }
    return self;
}

+ (BOOL)requiresMainQueueSetup
{
    return YES;
}

#pragma mark - Flyshot Delegate

- (void)flyshotPurchaseWithPaymentQueue:(SKPaymentQueue *)paymentQueue updatedTransactions:(NSArray<SKPaymentTransaction *> *)transactions {

    NSLog(@"Flyshot SDK purchase delegate called");

    NSMutableArray *output = [NSMutableArray array];
    
    for (SKPaymentTransaction *item in transactions) {

        NSNumber *transactionDate = @(item.transactionDate.timeIntervalSince1970 * 1000);
        NSString *transactionIdentifier = item.transactionIdentifier;
        NSString *paymentIdentifier = item.payment.productIdentifier;

        NSMutableDictionary *purchase = [[NSMutableDictionary alloc] initWithCapacity:4];
        [purchase setValue:transactionDate forKey:@"transactionDate"];
        [purchase setValue:transactionIdentifier forKey:@"transactionId"];
        [purchase setValue:paymentIdentifier forKey:@"productId"];
        [purchase setValue:[NSNumber numberWithInt:item.transactionState] forKey:@"transactionState"];

        if (purchase) {
            [output addObject:purchase];
        }
    }
    
    [channel invokeMethod:@"onFlyshotPurchase" arguments:@{@"transactions": output}];
}

- (BOOL)allowFlyshotPurchaseWithProductIdentifier:(NSString *)productIdentifier {

    NSLog(@"Flyshot SDK allowPurchase called with identifier %@", productIdentifier);

    if (blockedPurchases) {
        return ![blockedPurchases containsObject:productIdentifier];
    }
    return TRUE;
}

- (void)flyshotCampaignDetectedWithProductId:(NSString * _Nullable)productId {

    NSLog(@"Flyshot SDK campaignDetect called with product identifier %@", productId);
    
    [channel invokeMethod:@"onFlyshotCampaignDetected"
                arguments:@{@"productId": productId != nil ? productId : [NSNull null]}];
}


#pragma mark - Exported methods

- (void)handleMethodCall:(FlutterMethodCall*)call result:(FlutterResult)result {
  if ([@"initialize" isEqualToString:call.method]) {
      [self initialize:call result:result];
      
  } else if ([@"start" isEqualToString:call.method]) {
      [[Flyshot shared] start];
      
  } else if ([@"setAppsFlyerId" isEqualToString:call.method]) {
      NSString* appsflyerId = call.arguments[@"appsflyerId"];
      [Flyshot shared].appsflyerId = appsflyerId;
      
  } else if ([@"isFlyshotUser" isEqualToString:call.method]) {
      BOOL isFlyshotUser = [[Flyshot shared] isFlyshotUser];
      result([NSNumber numberWithBool:isFlyshotUser]);
      
  } else if ([@"isEligibleForDiscount" isEqualToString:call.method]) {
      BOOL isEligibleForDiscount = [[Flyshot shared] isEligibleForDiscount];
      result([NSNumber numberWithBool:isEligibleForDiscount]);
      
  } else if ([@"isEligibleForPromo" isEqualToString:call.method]) {
      BOOL isEligibleForPromo = [[Flyshot shared] isEligibleForPromo];
      result([NSNumber numberWithBool:isEligibleForPromo]);
      
  } else if ([@"isEligibleForReward" isEqualToString:call.method]) {
      BOOL isEligibleForReward = [[Flyshot shared] isEligibleForReward];
      result([NSNumber numberWithBool:isEligibleForReward]);
      
  } else if ([@"isCampaignActive" isEqualToString:call.method]) {
      NSString* campaignProductId = call.arguments[@"campaignProductId"];
      if ([campaignProductId isEqual:([NSNull null])]) {
          campaignProductId = nil;
      }
      BOOL isCampaignActive = [[Flyshot shared] isCampaignActiveWithProductId:campaignProductId];
      result([NSNumber numberWithBool:isCampaignActive]);
      
  } else if ([@"requestPromoBanner" isEqualToString:call.method]) {
      [[Flyshot shared] requestPromoBanner];
      
  } else if ([@"sendConversionEvent" isEqualToString:call.method]) {
      [self sendConversionEvent:call result:result];
      
  } else if ([@"sendEvent" isEqualToString:call.method]) {
      [self sendEvent:call result:result];
      
  } else if ([@"sendSignUpEvent" isEqualToString:call.method]) {
      [self sendSignUpEvent:call result:result];
      
  } else if ([@"upload" isEqualToString:call.method]) {
      [self upload:call result:result];
      
  } else if ([@"getContent" isEqualToString:call.method]) {
      [self getContent:call result:result];
      
  } else if ([@"setBlockedPurchasesList" isEqualToString:call.method]) {
      [self setBlockedPurchasesList:call result:result];
      
  } else {
    result(FlutterMethodNotImplemented);
  }
    
  result(nil);
}

- (void)initialize:(FlutterMethodCall*)call result:(FlutterResult)result {
    
    NSString* sdkToken = call.arguments[@"sdkToken"];
    
    [[Flyshot shared] initializeWithSdkToken:sdkToken onSuccess:^{
        result(nil);
    } onFailure:^(NSError * _Nonnull error) {
        result([FlutterError errorWithCode:[NSString stringWithFormat:@"Error %ld", error.code]
                                         message:error.domain
                                         details:error.localizedDescription]);
    }];
}

- (void)sendConversionEvent:(FlutterMethodCall*)call result:(FlutterResult)result {
    
    //double amount = call.arguments[@"conversionEventAmount"];
    double amount = [call.arguments[@"amount"] doubleValue];
    NSString* productId = call.arguments[@"conversionEventProductId"];
    if ([productId isEqual:([NSNull null])]) {
        productId = nil;
    }
    NSString* code = call.arguments[@"currencyCode"];
    if ([code isEqual:([NSNull null])]) {
        code = nil;
    }
    
    [[Flyshot shared] sendConversionEventWithAmount:amount currencyCode:code productId:productId onSuccess:^{
        result(nil);
    } onFailure:^(NSError * _Nonnull error) {
        result([FlutterError errorWithCode:[NSString stringWithFormat:@"Error %ld", error.code]
                                         message:error.domain
                                         details:error.localizedDescription]);
    }];
}

- (void)sendEvent:(FlutterMethodCall*)call result:(FlutterResult)result {
    
    NSString* name = call.arguments[@"eventName"];
    
    [[Flyshot shared] sendCustomEventWithName:name onSuccess:^{
        result(nil);
    } onFailure:^(NSError * _Nonnull error) {
        result([FlutterError errorWithCode:[NSString stringWithFormat:@"Error %ld", error.code]
                                         message:error.domain
                                         details:error.localizedDescription]);
    }];
}

- (void)sendSignUpEvent:(FlutterMethodCall*)call result:(FlutterResult)result {
    
    [[Flyshot shared] sendSignUpEventOnSuccess:^{
        result(nil);
    } onFailure:^(NSError * _Nonnull error) {
        result([FlutterError errorWithCode:[NSString stringWithFormat:@"Error %ld", error.code]
                                         message:error.domain
                                         details:error.localizedDescription]);
    }];
}

- (void)upload:(FlutterMethodCall*)call result:(FlutterResult)result {
    
    [[Flyshot shared] uploadOnSuccess:^(enum CampaignStatus status) {
        NSNumber *val = [NSNumber numberWithInteger:status];
        result(val);
    } onFailure:^(NSError * _Nonnull error) {
        result([FlutterError errorWithCode:[NSString stringWithFormat:@"Error %ld", error.code]
                                         message:error.domain
                                         details:error.localizedDescription]);
    }];
}

- (void)getContent:(FlutterMethodCall*)call result:(FlutterResult)result {
    
    [[Flyshot shared] getContentOnSuccess:^(NSArray<Post *> * _Nonnull content) {
            NSMutableDictionary *dict = [NSMutableDictionary dictionary];
            for (Post * post in content) {
                [dict setValue:post.creatorHandle forKey:@"creatorHandle"];
                [dict setValue:post.creatorName forKey:@"creatorName"];
                [dict setValue:post.caption forKey:@"caption"];
                [dict setValue:(post.imageURL ? post.imageURL.absoluteString : @"") forKey:@"imageURL"];
                [dict setValue:(post.thumbnailURL ? post.thumbnailURL.absoluteString : @"") forKey:@"thumbnailURL"];
                [dict setValue:(post.postLink ? post.postLink.absoluteString : @"") forKey:@"postLink"];
            }
            result(dict);
        } onFailure:^(NSError * _Nonnull error) {
            result([FlutterError errorWithCode:[NSString stringWithFormat:@"Error %ld", error.code]
                                             message:error.domain
                                             details:error.localizedDescription]);
        }];
}

- (void)setBlockedPurchasesList:(FlutterMethodCall*)call result:(FlutterResult)result {
    
    NSArray* purchases = call.arguments[@"purchases"];
    blockedPurchases = purchases;
}

@end
