#import <Flutter/Flutter.h>
#import <FlyshotSDK/FlyshotSDK-Swift.h>

@interface FlyshotSdkPlugin : NSObject<FlutterPlugin, FlyshotDelegate>
@end
