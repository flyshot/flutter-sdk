#
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html.
# Run `pod lib lint flyshot_sdk.podspec' to validate before publishing.
#
Pod::Spec.new do |s|
  s.name             = 'flyshot_sdk'
  s.version          = '0.1.0'
  s.summary          = 'Flyshot Flutter SDK.'
  s.description      = <<-DESC
Flutter plugin to integrate Flyshot IOS SDK to Flutter project.
                       DESC
  s.homepage         = 'https://flyshot.io'
  s.license          = { :file => '../LICENSE' }
  s.author           = { 'Flyshot inc.' => 'support@flyshot.io' }
  s.source           = { :path => '.' }
  s.source_files = 'Classes/**/*'
  s.public_header_files = 'Classes/**/*.h'
  s.dependency 'Flutter'
  s.platform = :ios, '10.0'

  # Flutter.framework does not contain a i386 slice.
  s.pod_target_xcconfig = { 'DEFINES_MODULE' => 'YES', 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'i386' }

  s.dependency 'Flyshot'
end
