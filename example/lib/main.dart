import 'dart:developer';

import 'package:flutter/material.dart';
import 'dart:async';

import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:flyshot_sdk/flyshot_sdk.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {

  // JWT for demo application
  //final String sdkToken = 'fl_prod_eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJjdXN0b21lclV1aWQiOiJiODdhNWI3MS00NzM4LTQyZDItYjlkZi04NjkwYThhN2NkZjkiLCJhcHBsaWNhdGlvblV1aWQiOiIzMDQxZmY5Yy1jZDk1LTQxMmItOWI4ZC03Njc4MjM4MzZjNDkiLCJ1dWlkIjoiMjk0OGNlODQtODhjNi00YzUwLTgwOWItYjE4MTNiNTI2MDRmIn0.78c_0kjMw4uGvxScw6DIyIg60v9l46edkmFB_dyhq2E';
  // App: Promo Internal
  final String sdkToken = 'fl_test_eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJjdXN0b21lclV1aWQiOiIwNDhhZGM4NC1kMzZiLTQ5NWYtOGZjNi01NTk4OTBhZjRmN2YiLCJhcHBsaWNhdGlvblV1aWQiOiJkOTdjOTM5Yi04MWRjLTRlNjEtYjNiOS1jYzgzZWYyYmIyMmUiLCJ1dWlkIjoiZGZkNTIxY2UtNmUyZi00M2FlLWE3NmQtNmRlNmE2MGY2ZWVjIn0.ds-414H8ZQYTgk0uKzRzRmPuQlOMrcSjE8mtjuKzjtI';
  //final String sdkToken = 'fl_dev_eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJjdXN0b21lclV1aWQiOiI5MTdlNjQ1MS1jMzI0LTRmOGMtYmZmNi05MjI1OTg4NDBiMjgiLCJhcHBsaWNhdGlvblV1aWQiOiIwYzYyZmU0My03MzE3LTRlYjQtOTU3MS1iNzJhNDk5ZjcwZWQiLCJ1dWlkIjoiNmYwN2U4YTUtZDljMS00NmUwLWEzMzYtOTU1ZjlmOWVlZTU4In0.Dn-KwnqIIeHpLU0nCOxoUZVZ4zlmi4YutUK2CLkPYl8';

  bool _initialized;
  bool _isFlyshotUser;
  bool _isEligibleForDiscount;
  bool _isEligibleForPromo;
  bool _isEligibleForReward;
  bool _isCampaignActive;

  @override
  void initState() {
    super.initState();
    initPlatformState();
  }

  // Platform messages are asynchronous, so we initialize in an async method.
  Future<void> initPlatformState() async {

    try {
      // PlatformException will be thrown if Flyshot init will fail
      await Flyshot.initialize(sdkToken);
      setState(() {
        _initialized = true;
      });

      await Flyshot.start();

      // Set AppsFlyerId
      // Flyshot.setAppsFlyerId('appsFlyerId');

      Flyshot.setBlockedPurchasesList(["Purchase1", "Purchase2"]);

      Flyshot.onFlyshotPurchase((params) async {
        log("onFlyshotPurchase event listener invoked with params " + params.toString());
      });

      Flyshot.onFlyshotCampaignDetected((params) async {
        log("onFlyshotCampaignDetected event listener invoked with params " + params.toString());
      });

      // Check SDK state periodicly
      Timer.periodic(const Duration(seconds: 3),  (timer) async {
        final isFlyshotUser = await Flyshot.isFlyshotUser();
        final isEligibleForDiscount = await Flyshot.isEligibleForDiscount();
        final isEligibleForPromo = await Flyshot.isEligibleForPromo();
        final isEligibleForReward = await Flyshot.isEligibleForReward();
        final isCampaignActive = await Flyshot.isCampaignActive(null);

        setState(() {
          _isFlyshotUser = isFlyshotUser;
          _isEligibleForDiscount = isEligibleForDiscount;
          _isEligibleForPromo = isEligibleForPromo;
          _isEligibleForReward = isEligibleForReward;
          _isCampaignActive = isCampaignActive;
        });
      });

    } on PlatformException catch (error) {
      log(error.toString());
      setState(() {
        _initialized = false;
      });
    }

    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
    if (!mounted) return;
  }

  void sendSignUp () {
    Flyshot.sendSignUpEvent().then((value) {
      showSuccessToast('Sign up sent successfully!');
    }).catchError((error) {
      showError('Sign up ERROR! ${error.toString()}');
      log(error.toString());
    });
  }

  void makeConversion (double amount) {
    Flyshot.sendConversionEvent(amount).then((_) {
      showSuccessToast('Conversion success!');
    }).catchError((error) {
      showError('Sign up ERROR! ${error.toString()}');
      log(error.toString());
    });
  }

  void sendCustomEvent (String eventName) {
    Flyshot.sendEvent(eventName).then((_) {
      showSuccessToast('$eventName sent successfully!');
    }).catchError((error) {
      showError('$eventName ERROR! ${error.toString()}');
      log(error.toString());
    });
  }

  void requestPromoBanner() {
    Flyshot.requestPromoBanner();
  }

  void upload () {
    Flyshot.upload().then((campaignStatus) {
      var status = '';
      switch (campaignStatus) {
        case 0:
          status = 'Not found!';
          break;
        case 1:
          status = 'Found!';
          break;
        case 2:
          status = 'Redeemed!';
          break;
      }

      showSuccessToast('Upload success with status: $status');
    }).catchError((error) {
      showError('Upload call ERROR! ${error.toString()}');
      log(error.toString());
    });
  }

  void getContent () {
    Flyshot.getContent().then((content) {
      showSuccessToast('Get content success!');
      log(content.toString());
    }).catchError((error) {
      showError('Get content ERROR! ${error.toString()}');
      log(error.toString());
    });
  }

  void showSuccessToast(String message) {
    Fluttertoast.showToast(
        msg: message,
        backgroundColor: Colors.green,
        textColor: Colors.white
    );
  }

  void showError(String error) {
    Fluttertoast.showToast(
        msg: error,
        backgroundColor: Colors.red,
        textColor: Colors.white
    );
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: const Text('Flyshot SDK Plugin example app'),
        ),
        body: Center(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Text('Initialized: ${null == _initialized ? 'Unspecified' : (_initialized ? 'Success' : 'Failed')}\n'),
                Text('isFlyshotUser: ${null == _isFlyshotUser ? 'Unspecified' : (_isFlyshotUser ? 'Yes' : 'No')}\n'),
                Text('isEligibleForDiscount: ${null == _isEligibleForDiscount ? 'Unspecified' : (_isEligibleForDiscount ? 'Yes' : 'No')}\n'),
                Text('isEligibleForPromo: ${null == _isEligibleForPromo ? 'Unspecified' : (_isEligibleForPromo ? 'Yes' : 'No')}\n'),
                Text('isEligibleForReward: ${null == _isEligibleForReward ? 'Unspecified' : (_isEligibleForReward ? 'Yes' : 'No')}\n'),
                Text('isCampaignActive: ${null == _isCampaignActive ? 'Unspecified' : (_isCampaignActive ? 'Yes' : 'No')}\n'),
                ElevatedButton(onPressed: () => makeConversion(19.99), child: Text('Add conversion \$19.99')),
                ElevatedButton(onPressed: () => sendCustomEvent('add-to-cart'), child: Text('Add to cart')),
                ElevatedButton(onPressed: () => sendCustomEvent('campaign-accepted'), child: Text('Accept campaign')),
                ElevatedButton(onPressed: sendSignUp, child: Text('Send Sign Up event')),
                ElevatedButton(onPressed: requestPromoBanner, child: Text('Request promo banner')),
                ElevatedButton(onPressed: upload, child: Text('Upload')),
                ElevatedButton(onPressed: getContent, child: Text('Get content')),
              ],
          )
        ),
      ),
    );
  }
}
