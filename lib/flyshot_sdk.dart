
import 'dart:async';
import 'package:flutter/services.dart';

typedef Future FlyshotCallback(dynamic params);

class Flyshot {

  static const MethodChannel _channel =
      const MethodChannel('flyshot_sdk');

  static FlyshotCallback _onFlyshotPurchase;
  static FlyshotCallback _onFlyshotCampaignDetected;

  static Future initialize(String sdkToken) async {
    Future f = _channel.invokeMethod('initialize', <String, String> {
      'sdkToken': sdkToken,
    });

    _channel.setMethodCallHandler(_didReceiveTranscript);
    
    return f;
  }

  static Future start() async {
    return _channel.invokeMethod('start');
  }

  static void onFlyshotPurchase(FlyshotCallback callback) {
    _onFlyshotPurchase = callback;
  }

  static void onFlyshotCampaignDetected(FlyshotCallback callback) {
    _onFlyshotCampaignDetected = callback;
  }

  static Future<void> _didReceiveTranscript(MethodCall call) async {
    // type inference will work here avoiding an explicit cast
    switch(call.method) {
      case "onFlyshotPurchase":
        _onFlyshotPurchase(call.arguments);
        break;
      case "onFlyshotCampaignDetected":
        _onFlyshotCampaignDetected(call.arguments);
        break;
    }
  }

  static Future setAppsFlyerId(String appsflyerId) async {
    return _channel.invokeMethod('setAppsFlyerId', <String, String> {
      'appsflyerId': appsflyerId,
    });
  }

  static Future<bool> isFlyshotUser() async {
    return _channel.invokeMethod('isFlyshotUser');
  }

  static Future<bool> isEligibleForDiscount() async {
    return _channel.invokeMethod('isEligibleForDiscount');
  }

  static Future<bool> isEligibleForPromo() async {
    return _channel.invokeMethod('isEligibleForPromo');
  }

  static Future<bool> isEligibleForReward() async {
    return _channel.invokeMethod('isEligibleForReward');
  }

  static Future<bool> isCampaignActive(String campaignProductId) async {
    return _channel.invokeMethod('isCampaignActive', <String, dynamic> {
      'campaignProductId': campaignProductId,
    });
  }

  static Future requestPromoBanner() async {
    return _channel.invokeMethod('requestPromoBanner');
  }

  static Future sendConversionEvent(double amount, [String conversionEventProductId, String currencyCode]) async {
    return _channel.invokeMethod('sendConversionEvent', <String, dynamic> {
      'amount': amount,
      'conversionEventProductId': conversionEventProductId,
      'currencyCode': currencyCode,
    });
  }

  static Future sendEvent(String eventName) async {
    return _channel.invokeMethod('sendEvent', <String, String> {
      'eventName': eventName,
    });
  }

  static Future sendSignUpEvent() async {
    return _channel.invokeMethod('sendSignUpEvent');
  }

  static Future<int> upload() async {
    return _channel.invokeMethod('upload');
  }

  static Future<dynamic> getContent() async {
    return _channel.invokeMethod('getContent');
  }

  static Future setBlockedPurchasesList(List<String> purchases) async {
    return _channel.invokeMethod('setBlockedPurchasesList', <String, dynamic> {
      'purchases': purchases,
    });
  }
}
